const { gql } = require("apollo-server");

const typeDefs = gql`
  type Usuario {
    id: ID
    nombre: String
    apellido: String
    email: String
    creado: String
  }

  type Producto {
    id: ID
    nombre: String
    existencia: Int
    status: String
    precio: Float
    creado: String
  }

  input ProductoInput {
    nombre: String!
    existencia: Int!
    precio: Float!
  }

  type Token {
    token: String
  }

  input UsuarioInput {
    nombre: String!
    apellido: String!
    email: String!
    password: String!
  }

  input autenticarInput {
    email: String!
    password: String!
  }

  input ProductoActualizarInput {
    id: ID!
    nombre: String
    existencia: Int
    precio: Float
    status: String
  }

  type Cliente {
    id: ID
    nombre: String
    apellido: String
    empresa: String
    email: String
    telefono: String
    vendedor: ID
    status: String
  }

  input ClienteInput {
    nombre: String!
    apellido: String!
    empresa: String!
    email: String!
    telefono: String
  }

  input ClienteActualizarInput {
    id: ID!
    nombre: String
    apellido: String
    empresa: String
    email: String
    telefono: String
    status: String
  }
  type Query {
    # Usuarios
    obtenerUsuario(token:String): Usuario
    # Productos
    obtenerProductos: [Producto]
    obtenerProducto(id:ID!): Producto
    # Clientes
    obtenerClientes: [Cliente]
    obtenerClientesVendedor: [Cliente]
    obtenerCliente(id: ID!): Cliente
  }

  type Mutation {
    # Usuarios
    nuevoUsuario(input: UsuarioInput!): Usuario!
    autenticarUsuario(input:autenticarInput): Token
    # Productos
    nuevoProducto(input: ProductoInput): Producto
    actualizarProducto(input:ProductoActualizarInput): Producto
    eliminarProducto(id:ID!): String
    # Clientes
    nuevoCliente(input: ClienteInput): Cliente
    actualizarCliente(input: ClienteActualizarInput): Cliente
    eliminarCliente(id: ID!): String
  }
`

module.exports = typeDefs;
