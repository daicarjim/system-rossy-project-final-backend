const Usuario = require("../models/Usuario");
const Producto = require("../models/Producto");
const Cliente = require("../models/Cliente");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
//Resolvers

const crearToken = (usuario, secreta, expiresIn) => {
  console.log(usuario);
  const { id, email, nombre, apellido } = usuario;

  return jwt.sign({ id, email, nombre, apellido }, secreta, { expiresIn });
};

const resolvers = {
  Query: {
    obtenerUsuario: async (_, { token }) => {
      const usuarioId = await jwt.verify(token, process.env.SECRETA);
      return usuarioId;
    },
    obtenerProductos: async () => {
      try {
        const productos = await Producto.find({});
        return productos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerProducto: async (_, { id }) => {
      try {
        // Revisar si el producto existe o no

        const producto = await Producto.findById(id);
        if (!producto) {
          throw new Error("Producto no encontrado");
        }

        return producto;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al buscar el producto");
      }
    },
    obtenerClientes: async () => {
      try {
        const clientes = await Cliente.find({});
        return clientes;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al buscar los clientes");
      }
    },
    obtenerClientesVendedor: async (_, {}, ctx) => {
      try {
        const clientes = await Cliente.find({
          vendedor: ctx.usuario.id.toString(),
        });
        return clientes;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al buscar los clientes");
      }
    },
    obtenerCliente: async (_, { id }, ctx) => {
      const cliente = await Cliente.findById(id);

      if (!cliente) {
        throw new Error("Cliente no encontrado");
      }
      //arreglar filtro
      if (cliente.vendedor.toString() !== ctx.usuario.id) {
        throw new Error("No tienes las credenciales");
      }
      return cliente;
    },
  },
  Mutation: {
    nuevoUsuario: async (_, { input }) => {
      const { email, password } = input;
      // Revisar si el usuario ya esta registrado
      const existeUsuario = await Usuario.findOne({ email });
      if (existeUsuario) {
        throw new Error("El usuario ya esta registrado");
      }
      //Hashear su password

      const salt = await bcryptjs.genSalt(10);
      input.password = await bcryptjs.hash(password, salt);

      try {
        //Guardarlo en la base de datos
        const usuario = new Usuario(input);
        usuario.save();
        return usuario;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al crear el usuario");
      }
    },

    autenticarUsuario: async (_, { input }) => {
      // Si el usuario existe
      const { email, password } = input;

      const existeUsuario = await Usuario.findOne({ email });
      if (!existeUsuario) {
        throw new Error("El usuario no existe");
      }

      // Revisar si el password es correcto
      const passwordCorrecto = await bcryptjs.compare(
        password,
        existeUsuario.password
      );
      if (!passwordCorrecto) {
        throw new Error("El password es incorrecto");
      }

      // Crear el token
      return {
        token: crearToken(existeUsuario, process.env.SECRETA, "24h"),
      };
    },

    nuevoProducto: async (_, { input }) => {
      try {
        const producto = new Producto(input);
        const resultado = await producto.save();
        const updateStatus = await Producto.findByIdAndUpdate(
          { _id: resultado._id },
          { status: "ACTIVO" },
          { new: true }
        );
        return resultado;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al crear el producto");
      }
    },
    actualizarProducto: async (_, { input }) => {
      try {
        const { id, ...resto } = input;
        let producto = await Producto.findById(id);
        if (!producto) {
          throw new Error("Producto no encontrado");
        }

        producto = await Producto.findByIdAndUpdate({ _id: id }, resto, {
          new: true,
        });

        return producto;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al actualizar el producto");
      }
    },

    eliminarProducto: async (_, { id }) => {
      try {
        let producto = await Producto.findById(id);
        if (!producto) {
          throw new Error("Producto no encontrado");
        }

        const productoStatus = await Producto.findByIdAndUpdate(
          (_id = id),
          { status: "DELETED" },
          { new: true }
        );
        return "Producto eliminado";
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al eliminar el producto");
      }
    },
    nuevoCliente: async (_, { input }, ctx) => {
      //verificar si el cliente esta registrado
      const { email } = input;
      const cliente = await Cliente.findOne({ email });
      if (cliente) {
        throw new Error("El cliente ya esta registrado");
      }
      const nuevoCliente = new Cliente(input);
      //asignar vendedor

      nuevoCliente.status = "ACTIVO";

      nuevoCliente.vendedor = ctx.usuario.id;
      //console.log(nuevoCliente.vendedor);
      //guardarlo en la base de datos
      try {
        const resultado = await nuevoCliente.save();
        return resultado;
      } catch (error) {
        console.log(error);
        throw new Error("Hubo un error al crear el cliente");
      }
    },
    actualizarCliente: async (_, { input }, ctx) => {
      const { id, ...resto } = input;
      try {
        //verificar si existe o no
        const cliente = await Cliente.findById(id);
        if (!cliente) {
          throw new Error("Cliente no encontrado");
        }
        //verificar si el vendedor es quien edita
        if (cliente.vendedor.toString() !== ctx.usuario.id) {
          throw new Error("No tienes las credenciales");
        }
        //actualizarlo
        const clienteActualizado = await Cliente.findOneAndUpdate(
          { _id: id },
          resto,
          {
            new: true,
          }
        );

        return clienteActualizado;
      } catch (error){
        console.log(error);
        throw new Error("Hubo un error al actualizar el cliente");
      }
    },
    eliminarCliente: async (_, { id }, ctx) => {
      try {
        //verificar si existe o no
        const cliente = await Cliente.findById(id);
        if (!cliente) {
          throw new Error("Cliente no encontrado");
        }
        //verificar si el vendedor es quien edita

        if (cliente.vendedor.toString() !== ctx.usuario.id) {
          throw new Error("No tienes las credenciales");
        }
        //actualizarlo
        const clienteActualizado = await Cliente.findOneAndUpdate(
          { _id: id },
          { status: "DELETED" },
          {
            new: true,
          }
        );
        return "Cliente eliminado";
      } catch (error){
        console.log(error);
        throw new Error("Hubo un error al eliminar el cliente");
      }
    }
  },
};

module.exports = resolvers;
